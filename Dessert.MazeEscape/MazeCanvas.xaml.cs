﻿//
// MazeCanvas.xaml.cs
//  
// Author(s):
//       Alessio Parma <alessio.parma@gmail.com>
// 
// Copyright (c) 2014 Alessio Parma <alessio.parma@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using Dessert.MazeEscape.Common;
using System.Windows.Media;
using Dessert.MazeEscape.Processes;

namespace Dessert.MazeEscape
{
    /// <summary>
    ///   Interaction logic for MazeCanvas.xaml.
    /// </summary>
    public partial class MazeCanvas : UserControl
    {
        readonly Dictionary<EscaperBase, Shape> _escapers = new Dictionary<EscaperBase, Shape>(); 

        public Maze _currentMaze;

        public MazeCanvas()
        {
            InitializeComponent();
        }

        public void DrawMaze(Maze maze)
        {
            // We are going to draw maze again, even if it has been drawn.
            _currentMaze = null;

            // We read default styles...
            var mazeBackground = (Brush) Application.Current.Resources["MazeBackground"];
            var mazeWallColor = (Brush) Application.Current.Resources["MazeWallColor"];
            var mazeLineThickness = double.Parse(AppSettings.MazeLineThickness);

            // First of all, we clear the canvas.
            Canvas.Children.Clear();
            Canvas.Background = mazeBackground;

            // Cell dimensions are computed from canvas size.
            var cellWidth = Canvas.ActualWidth / maze.Width;
            var cellHeight = Canvas.ActualHeight / maze.Height;         

            // Useful methods to compute cell offsets.
            Func<uint, double> xOffset = x => x * cellWidth;
            Func<uint, double> yOffset = y => y * cellHeight;

            // Useful methods to draw cell walls.
            Action<Cell, WallPosition, double, double, double, double> drawWall = (c, w, x1, y1, x2, y2) => {
                if (!c.HasWall(w)) {
                    return;
                }
                var wall = new Line {
                    X1 = x1,
                    Y1 = y1,
                    X2 = x2,
                    Y2 = y2,
                    Stroke = mazeWallColor,
                    StrokeStartLineCap = PenLineCap.Round,
                    StrokeEndLineCap = PenLineCap.Round,
                    StrokeThickness = mazeLineThickness,
                };
                Canvas.Children.Add(wall);
            };
            Action<Cell> drawWalls = c => {
                var oX = xOffset(c.X);
                var oY = yOffset(c.Y);
                drawWall(c, WallPosition.West, oX, oY, oX, oY + cellHeight);
                drawWall(c, WallPosition.North, oX, oY, oX + cellWidth, oY);
            };

            // Now, for each cell, we draw a proper rectangle inside the canvas.
            // We do not draw a single rectangle, because each cell has missing walls.
            for (var y = 0; y <= maze.Height; ++y) {
                for (var x = 0; x <= maze.Width; ++x) {
                    var cell = maze.Cells[y, x];
                    drawWalls(cell);
                }
            }

            // At last, we mark the fact that the maze is ready.
            _currentMaze = maze;
        }

        public void DrawEscaper(EscaperBase escaper)
        {
            Debug.Assert(_currentMaze != null, "A maze must be drawn first!");
            // The figure corresponding to the escaper.
            var figure = new Ellipse();
            // We register the escaper and the corresponding figure;
            // we will use the reference to the figure to move the escaper around.
            _escapers.Add(escaper, figure);
            // The escaper is drawn inside the entrance.
            PositionEscaperInsideCell(figure, _currentMaze.Entrance);
            Canvas.Children.Add(figure);
        }

        public void MoveEscaper(EscaperBase escaper, Cell cell)
        {
            PositionEscaperInsideCell(_escapers[escaper], cell);
            Canvas.UpdateLayout();
        }

        void PositionEscaperInsideCell(Shape figure, Cell cell)
        {
            // Cell dimensions are computed from canvas size.
            var cellWidth = Canvas.ActualWidth / _currentMaze.Width;
            var cellHeight = Canvas.ActualHeight / _currentMaze.Height;

            // Useful methods to compute cell offsets.
            Func<uint, double> xOffset = x => x * cellWidth;
            Func<uint, double> yOffset = y => y * cellHeight;

            figure.Fill = new SolidColorBrush(Colors.Magenta);
            figure.SetValue(Canvas.LeftProperty, xOffset(cell.X));
            figure.SetValue(Canvas.TopProperty, yOffset(cell.Y));
            figure.Width = figure.Height = 15;
        }
    }
}
