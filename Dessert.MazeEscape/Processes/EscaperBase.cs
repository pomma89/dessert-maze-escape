﻿//
// EscaperBase.cs
//  
// Author(s):
//       Alessio Parma <alessio.parma@gmail.com>
// 
// Copyright (c) 2014 Alessio Parma <alessio.parma@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Dessert.MazeEscape.Common;
using Dessert.Unchecked;
using Timeout = Dessert.Unchecked.Events.Timeout;

namespace Dessert.MazeEscape.Processes
{
    public abstract class EscaperBase
    {
        const double TimeUnit = 1.0;

        readonly SimEnvironment _env;
        readonly Binder _binder;
        readonly string _name;

        protected EscaperBase(SimEnvironment env, Binder binder)
        {
            _env = env;
            _binder = binder;
            _name = GetType().Name;
            // Speed is set to its default value.
            Speed = double.Parse(AppSettings.DefaultEscaperSpeed);
        }

        /// <summary>
        ///   The speed at which the escaper can move.
        /// </summary>
        public double Speed { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="maze">The maze from which we should escape.</param>
        public IEnumerable<SimEvent> TryToEscape(Maze maze)
        {
            var currentCell = maze.Entrance;
            _binder.PrintTrace("{0} is entering the maze, good luck to him!", _name);
            while (!ReferenceEquals(currentCell, maze.Exit)) {
                var nextCell = ChooseNextCell(currentCell);
                yield return MoveToCell(currentCell, nextCell);
                _binder.PrintTrace("{0} moved ({1}, {2})...", _name, nextCell.X, nextCell.Y);
                _binder.MoveEscaper(this, nextCell);
                Thread.Sleep(100);
                currentCell = nextCell;
            }
            _binder.PrintTrace("{0} exited the maze at {1}! Amazing :)", _name, _env.Now);
        }

        /// <summary>
        /// 
        /// </summary>
        protected abstract Cell ChooseNextCell(Cell actual);

        /// <summary>
        /// 
        /// </summary>
        protected Timeout MoveToCell(Cell from, Cell to)
        {
            // We compute move time and we check if it is a valid number.
            var time = TimeUnit / Speed;
            Debug.Assert(!double.IsNaN(time) && !double.IsInfinity(time));
            // At last, a timeout event is created and returned.
            return _env.Timeout(time);
        }
    }
}
