﻿//
// Cell.cs
//  
// Author(s):
//       Alessio Parma <alessio.parma@gmail.com>
// 
// Copyright (c) 2014 Alessio Parma <alessio.parma@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Diagnostics;

namespace Dessert.MazeEscape.Common
{
    public sealed class Cell
    {
        public readonly uint X, Y;
        
        readonly Maze _maze;
        bool _hasWestWall, _hasNorthWall;

        public Cell(Maze maze, uint x, uint y)
        {
            _maze = maze;
            X = x;
            Y = y;
            // Rightmost cells have no north walls.
            _hasNorthWall = (X != maze.Width);
            // Lowermost cells have no west walls.
            _hasWestWall = (Y != maze.Height);
        }

        public void EraseWall(WallPosition wall)
        {
            Debug.Assert(Enum.IsDefined(typeof(WallPosition), wall));
            Debug.Assert(!IsMazeBorder(wall));
            switch (wall) {
                case WallPosition.West:
                    _hasWestWall = false;
                    break;
                case WallPosition.North:
                    _hasNorthWall = false;
                    break;
                case WallPosition.East:
                    // Never called from rightmost cells.
                    _maze.Cells[Y, X+1]._hasWestWall = false;
                    break;
                case WallPosition.South:
                    // Never called from lowermost cells.
                    _maze.Cells[Y + 1, X]._hasNorthWall = false;
                    break;
                default:
                    throw new Exception("Invalid wall position");
            }
        }

        public Cell GetNeighbour(WallPosition wall)
        {
            Debug.Assert(Enum.IsDefined(typeof(WallPosition), wall));
            switch (wall) {
                case WallPosition.West:
                    Debug.Assert(X > 0);
                    return _maze.Cells[Y, X - 1];
                case WallPosition.North:
                    Debug.Assert(Y > 0);
                    return _maze.Cells[Y - 1, X];
                case WallPosition.East:
                    Debug.Assert(X < _maze.Width - 1);
                    return _maze.Cells[Y, X + 1];
                case WallPosition.South:
                    Debug.Assert(Y < _maze.Height - 1);
                    return _maze.Cells[Y + 1, X];
                default:
                    throw new Exception("Invalid wall position");
            }
        }

        public bool HasWall(WallPosition wall)
        {
            Debug.Assert(Enum.IsDefined(typeof(WallPosition), wall));
            switch (wall) {
                case WallPosition.West:
                    return _hasWestWall;
                case WallPosition.North:
                    return _hasNorthWall;
                case WallPosition.East:
                    // Never called from rightmost cells.
                    return _maze.Cells[Y, X + 1]._hasWestWall;
                case WallPosition.South:
                    // Never called from lowermost cells.
                    return _maze.Cells[Y + 1, X]._hasNorthWall;
                default:
                    throw new Exception("Invalid wall position");
            }
        }

        public bool IsMazeBorder(WallPosition wall)
        {
            Debug.Assert(Enum.IsDefined(typeof(WallPosition), wall));
            switch (wall) {
                case WallPosition.West:
                    return X == 0;
                case WallPosition.North:
                    return Y == 0;
                case WallPosition.East:
                    // Never called from rightmost cells.
                    return X == _maze.Width - 1;
                case WallPosition.South:
                    // Never called from lowermost cells.
                    return Y == _maze.Height - 1;
                default:
                    throw new Exception("Invalid wall position");
            }
        }
    }
}
