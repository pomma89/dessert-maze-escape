﻿//
// Maze.cs
//  
// Author(s):
//       Alessio Parma <alessio.parma@gmail.com>
// 
// Copyright (c) 2014 Alessio Parma <alessio.parma@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Troschuetz.Random;

namespace Dessert.MazeEscape.Common
{
    public sealed class Maze
    {
        static readonly WallPosition[][] AllNeighbours = {
            new[] /* 00 */ {WallPosition.None /* Dummy, never used */},
            new[] /* 01 */ {WallPosition.West},
            new[] /* 02 */ {WallPosition.North},
            new[] /* 03 */ {WallPosition.North, WallPosition.West},
            new[] /* 04 */ {WallPosition.East},
            new[] /* 05 */ {WallPosition.East, WallPosition.West},
            new[] /* 06 */ {WallPosition.East, WallPosition.North},
            new[] /* 07 */ {WallPosition.East, WallPosition.North, WallPosition.West},
            new[] /* 08 */ {WallPosition.South},
            new[] /* 09 */ {WallPosition.South, WallPosition.West},
            new[] /* 10 */ {WallPosition.South, WallPosition.North},
            new[] /* 11 */ {WallPosition.South, WallPosition.North, WallPosition.West},
            new[] /* 12 */ {WallPosition.South, WallPosition.East},
            new[] /* 13 */ {WallPosition.South, WallPosition.East, WallPosition.West},
            new[] /* 14 */ {WallPosition.South, WallPosition.East, WallPosition.North},
            new[] /* 15 */ {WallPosition.South, WallPosition.East, WallPosition.North, WallPosition.West}
        };

        public readonly Cell[,] Cells;
        public readonly uint Width, Height;
        public readonly Cell Entrance, Exit;

        readonly TRandom _random = new TRandom();   

        public Maze(uint width, uint height)
        {
            Debug.Assert(width > 0 && height > 0);
            Width = width;
            Height = height;
            Cells = AllocateCells();          
            GenerateMaze();
            // At last, we create entrance and exit.
            Entrance = Cells[0, 0];
            Exit = Cells[height - 1, width - 1];
        }

        public override string ToString()
        {
            const char entrance = 'S';
            const char exit = 'E';
            const char westWall = '|';
            const char northWall = '¯';
            const char blank = ' ';
            var maze = new StringBuilder();
            for (var y = 0; y <= Height; ++y) {
                for (var x = 0; x <= Width; ++x) {
                    var cell = Cells[y, x];
                    maze.Append(cell.HasWall(WallPosition.West) ? westWall : blank);
                    if (ReferenceEquals(cell, Entrance)) {
                        maze.Append(entrance);
                    } else if (ReferenceEquals(cell, Exit)) {
                        maze.Append(exit);
                    } else { 
                        maze.Append(cell.HasWall(WallPosition.North) ? northWall : blank);
                    }                   
                }
                maze.AppendLine();
            }
            return maze.ToString();
        }

        Cell[,] AllocateCells()
        {
            // We need to allocate one more row/column,
            // in order to efficiently handle walls and borders.
            var allocWidth = Width + 1;
            var allocHeight = Height + 1;
            // Maze cells are allocated using new measures.
            var cells = new Cell[allocHeight, allocWidth];
            for (var h = 0U; h < allocHeight; ++h) {
                for (var w = 0U; w < allocWidth; ++w) {
                    cells[h, w] = new Cell(this, w, h);
                }
            }
            return cells;
        }

        /// <summary>
        ///   Generates a maze using the DFS approach.
        ///   In particular, we use the recursive backtracker.
        /// </summary>
        void GenerateMaze()
        {
            // We need to store the number of visited cells and which cells have been visited.
            var allCells = Width * Height;
            var visitedCount = 0U;
            var visitInfo = new bool[Height, Width];
            var cellStack = new Stack<Cell>();

            // A function to mark a cell as visited.
            Action<Cell> visitCell = c => { visitedCount++; visitInfo[c.Y, c.X] = true; };

            // A function to pick a random unvisited neighbour.
            Func<Cell, WallPosition> randomUnvisitedNeighbour = c => {
                var result = WallPosition.None;
                if (c.X > 0 && !visitInfo[c.Y, c.X - 1]) result |= WallPosition.West;
                if (c.Y > 0 && !visitInfo[c.Y - 1, c.X]) result |= WallPosition.North;
                if (c.X < Width - 1 && !visitInfo[c.Y, c.X + 1]) result |= WallPosition.East;
                if (c.Y < Height - 1 && !visitInfo[c.Y + 1, c.X]) result |= WallPosition.South;
                // Following instruction checks whether this cell has neighbours.
                // If it doesn't, then we return a dummy position.
                if (result == WallPosition.None) return WallPosition.None;
                // Else, we pick a random neighbour between the ones available.
                return _random.Choice(AllNeighbours[(byte) result]);
            };

            // A function to pick a random unvisited cell.
            Func<Cell> randomUnvisitedCell = () => {
                const byte maxCellCount = 10;
                // We collect at most maxCellCount cells and then we return a random one.
                var unvisitedCells = new List<Cell>(maxCellCount);
                for (var y = 0; y < Height; ++y) {
                    for (var x = 0; x <= Width; ++x) {
                        if (!visitInfo[y, x]) {
                            unvisitedCells.Add(Cells[y, x]);
                        }
                        if (unvisitedCells.Count == maxCellCount) {
                            goto endSearch;
                        }
                    }             
                }
            endSearch:
                return _random.Choice(unvisitedCells);
            };

            // We start by setting a random cell as a start point.
            // We skip first and last rows/columns, to avoid the situation
            // in which the source cell is the exit; in fact, by using the DFS approach,
            // every path leads to the source cell, which would make it extremely
            // easy to find the way out should they be the same.
            var randX = _random.Next(1, (int) Width);
            var randY = _random.Next(1, (int) Height);
            
            // The start cell is visited.
            var currCell = Cells[randY, randX];
            visitCell(currCell);

            // Now, we apply the recursive backtrack algorithm.
            while (visitedCount < allCells) {
                // Checks whether current cell has any neighbours which have not been visited.
                var rndUnvisitedNeighbour = randomUnvisitedNeighbour(currCell);
                if (rndUnvisitedNeighbour != WallPosition.None) {
                    // It has, so we push the current to the stack.
                    cellStack.Push(currCell);
                    // We remove the wall between the current cell and the chosen cell.
                    currCell.EraseWall(rndUnvisitedNeighbour);
                    // At last, we make the chosen cell the current cell and we mark it as visited.
                    currCell = currCell.GetNeighbour(rndUnvisitedNeighbour);
                    visitCell(currCell);
                } else if (cellStack.Count > 0) {
                    // All neighbours were visited, so we pick a cell from the stack.
                    currCell = cellStack.Pop();
                } else {
                    currCell = randomUnvisitedCell();
                    visitCell(currCell);
                }
            }
        }
    }
}
