﻿//
// MainPage.xaml.cs
//  
// Author(s):
//       Alessio Parma <alessio.parma@gmail.com>
// 
// Copyright (c) 2014 Alessio Parma <alessio.parma@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Threading.Tasks;
using System.Windows;
using Dessert.MazeEscape.Common;
using Dessert.MazeEscape.Processes;
using Dessert.Unchecked;

namespace Dessert.MazeEscape
{
    /// <summary>
    ///   Interaction logic for MainPage.xaml.
    /// </summary>
    public partial class MainPage
    {
        static readonly uint DefaultMazeSide = UInt32.Parse(AppSettings.DefaultMazeSide);
        static readonly double DefaultSimulationTime = Double.Parse(AppSettings.DefaultSimulationTime);

        public MainPage()
        {
            InitializeComponent();
        }

        private void BtnPrintMaze_Click(object sender, RoutedEventArgs e)
        {
            var maze = new Maze(DefaultMazeSide, DefaultMazeSide);
            MazeCanvas.DrawMaze(maze);
        }

        private void BtnStartStopSim_Click(object sender, RoutedEventArgs e)
        {
            var binder = new Binder(this);
            var env = Sim.NewEnvironment();

            // A quick method to register the escaper inside the maze and the simulation.
            Action<EscaperBase> registerEscaper = es => {
                MazeCanvas.DrawEscaper(es);
                var pem = es.TryToEscape(MazeCanvas._currentMaze);
                env.Process(pem);
            };

            registerEscaper(new RandomMouse(env, binder));

            // We run simulation in another thread, to allow UI to be responsive.
            Task.Factory.StartNew(t => env.Run((double) t), DefaultSimulationTime);
        }
    }
}
